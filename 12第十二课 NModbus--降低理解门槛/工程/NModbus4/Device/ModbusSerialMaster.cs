namespace Modbus.Device
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.IO.Ports;
    using System.Net.Sockets;

    using Data;
    using IO;
    using Message;

    /// <summary>
    ///   Modbus串行主设备类。
    /// </summary>
    public class ModbusSerialMaster : ModbusMaster, IModbusSerialMaster
    {
        /// <summary>
        /// 初始化modbus串行主设备实例。
        /// </summary>
        /// <param name="transport"></param>
        private ModbusSerialMaster(ModbusTransport transport)
            : base(transport)
        {
        }

        /// <summary>
        /// 获取modbus串行主设备传输对象。
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        ModbusSerialTransport IModbusSerialMaster.Transport
        {
            get { return (ModbusSerialTransport) Transport; }
        }

        /// <summary>
        /// Modbus Ascii主设备SerialPort传输工厂方法。
        /// </summary>
        public static ModbusSerialMaster CreateAscii(SerialPort serialPort)
        {
            if (serialPort == null)
                throw new ArgumentNullException("serialPort");

            return CreateAscii(new SerialPortAdapter(serialPort));
        }

        /// <summary>
        ///  Modbus Ascii主设备TcpClient传输工厂方法。
        /// </summary>
        public static ModbusSerialMaster CreateAscii(TcpClient tcpClient)
        {
            if (tcpClient == null)
                throw new ArgumentNullException("tcpClient");

            return CreateAscii(new TcpClientAdapter(tcpClient));
        }

        /// <summary>
        ///  Modbus Ascii主设备UdpClient传输工厂方法。
        /// </summary>
        public static ModbusSerialMaster CreateAscii(UdpClient udpClient)
        {
            if (udpClient == null)
                throw new ArgumentNullException("udpClient");
            if (!udpClient.Client.Connected)
                throw new InvalidOperationException(Resources.UdpClientNotConnected);

            return CreateAscii(new UdpClientAdapter(udpClient));
        }

        /// <summary>
        ///  Modbus Ascii主设备IStreamResource传输工厂方法。
        /// </summary>
        public static ModbusSerialMaster CreateAscii(IStreamResource streamResource)
        {
            if (streamResource == null)
                throw new ArgumentNullException("streamResource");

            return new ModbusSerialMaster(new ModbusAsciiTransport(streamResource));
        }

        /// <summary>
        ///  Modbus RTU主设备SerialPort传输工厂方法。
        /// </summary>
        public static ModbusSerialMaster CreateRtu(SerialPort serialPort)
        {
            if (serialPort == null)
                throw new ArgumentNullException("serialPort");

            return CreateRtu(new SerialPortAdapter(serialPort));
        }

        /// <summary>
        /// Modbus RTU主设备TcpClient传输工厂方法。
        /// </summary>
        public static ModbusSerialMaster CreateRtu(TcpClient tcpClient)
        {
            if (tcpClient == null)
                throw new ArgumentNullException("tcpClient");

            return CreateRtu(new TcpClientAdapter(tcpClient));
        }

        /// <summary>
        ///  Modbus RTU主设备UdpClient传输工厂方法。
        /// </summary>
        public static ModbusSerialMaster CreateRtu(UdpClient udpClient)
        {
            if (udpClient == null)
                throw new ArgumentNullException("udpClient");
            if (!udpClient.Client.Connected)
                throw new InvalidOperationException(Resources.UdpClientNotConnected);

            return CreateRtu(new UdpClientAdapter(udpClient));
        }

        /// <summary>
        ///  Modbus RTU主设备IStreamResource传输工厂方法。
        /// </summary>
        public static ModbusSerialMaster CreateRtu(IStreamResource streamResource)
        {
            if (streamResource == null)
                throw new ArgumentNullException("streamResource");

            return new ModbusSerialMaster(new ModbusRtuTransport(streamResource));
        }

        /// <summary>
        /// 只支持串行通讯
        /// 诊断函数，它循环回原始数据。 
        /// NModbus只支持回环一个ushort值，这是RTU协议的“Best Effort”实现的一个限制。
        /// </summary>
        /// <param name="slaveAddress">要测试设备的地址</param>
        /// <param name="data">返回的值</param>
        /// <returns>如果从设备回显数据，返回true。</returns>
        public bool ReturnQueryData(byte slaveAddress, ushort data)
        {
            DiagnosticsRequestResponse request = new DiagnosticsRequestResponse(Modbus.DiagnosticsReturnQueryData,
                slaveAddress, new RegisterCollection(data));
            DiagnosticsRequestResponse response = Transport.UnicastMessage<DiagnosticsRequestResponse>(request);

            return response.Data[0] == data;
        }
    }
}
