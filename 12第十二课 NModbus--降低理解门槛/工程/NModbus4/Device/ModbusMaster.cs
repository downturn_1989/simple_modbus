namespace Modbus.Device
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;

    using Data;
    using IO;
    using Message;

    /// <summary>
    ///  Modbus主设备抽象类，主要对<see cref="IModbusMaster"/>接口的实现。
    ///  扩展思考：抽象类对接口实现在程序框架的优势。
    /// </summary>
    public abstract class ModbusMaster : ModbusDevice, IModbusMaster
    {
        /// <summary>
        /// 初始化Modbus主设备实例
        /// </summary>
        /// <param name="transport">传输对象</param>
        internal ModbusMaster(ModbusTransport transport)
            : base(transport)
        {
        }

        /// <summary>
        /// 读取从1到2000的线圈状态
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>线圈数据</returns>
        public bool[] ReadCoils(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            ValidateNumberOfPoints("numberOfPoints", numberOfPoints, 2000);

            var request = new ReadCoilsInputsRequest(Modbus.ReadCoils, slaveAddress, startAddress, numberOfPoints);
            return PerformReadDiscretes(request);
        }

        /// <summary>
        /// 异步读取从1到2000的线圈状态   
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>返回异步读操作的任务</returns>
        public Task<bool[]> ReadCoilsAsync(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            ValidateNumberOfPoints("numberOfPoints", numberOfPoints, 2000);

            var request = new ReadCoilsInputsRequest(Modbus.ReadCoils, slaveAddress, startAddress, numberOfPoints);
            return PerformReadDiscretesAsync(request);
        }

        /// <summary>
        ///    读取1到2000离散输入的状态
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>离散数据</returns>
        public bool[] ReadInputs(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            ValidateNumberOfPoints("numberOfPoints", numberOfPoints, 2000);

            var request = new ReadCoilsInputsRequest(Modbus.ReadInputs, slaveAddress, startAddress, numberOfPoints);
            return PerformReadDiscretes(request);
        }

        /// <summary>
        ///    读取1到2000离散输入的状态
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>返回异步读操作的任务</returns>
        public Task<bool[]> ReadInputsAsync(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            ValidateNumberOfPoints("numberOfPoints", numberOfPoints, 2000);

            var request = new ReadCoilsInputsRequest(Modbus.ReadInputs, slaveAddress, startAddress, numberOfPoints);
            return PerformReadDiscretesAsync(request);
        }

        /// <summary>
        ///    读取保持寄存器数据。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>保持寄存器数据</returns>
        public ushort[] ReadHoldingRegisters(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            ValidateNumberOfPoints("numberOfPoints", numberOfPoints, 125);

            var request = new ReadHoldingInputRegistersRequest(
                Modbus.ReadHoldingRegisters,
                slaveAddress,
                startAddress,
                numberOfPoints);
            return PerformReadRegisters(request);
        }

        /// <summary>
        ///    异步读取保持寄存器数据。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>返回异步读操作的任务</returns>
        public Task<ushort[]> ReadHoldingRegistersAsync(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            ValidateNumberOfPoints("numberOfPoints", numberOfPoints, 125);

            var request = new ReadHoldingInputRegistersRequest(
                Modbus.ReadHoldingRegisters,
                slaveAddress,
                startAddress,
                numberOfPoints);
            return PerformReadRegistersAsync(request);
        }

        /// <summary>
        /// 读取输入寄存器数据。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>输入寄存器数据</returns>
        public ushort[] ReadInputRegisters(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            ValidateNumberOfPoints("numberOfPoints", numberOfPoints, 125);

            var request = new ReadHoldingInputRegistersRequest(
                Modbus.ReadInputRegisters,
                slaveAddress,
                startAddress,
                numberOfPoints);
            return PerformReadRegisters(request);
        }

        /// <summary>
        /// 异步读取输入寄存器数据。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>返回异步读操作的任务</returns>
        public Task<ushort[]> ReadInputRegistersAsync(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
        {
            ValidateNumberOfPoints("numberOfPoints", numberOfPoints, 125);

            var request = new ReadHoldingInputRegistersRequest(
                Modbus.ReadInputRegisters,
                slaveAddress,
                startAddress,
                numberOfPoints);
            return PerformReadRegistersAsync(request);
        }

        /// <summary>
        ///  写单个线圈。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="coilAddress">要写入起始地址</param>
        /// <param name="value">要写入的值</param>
        public void WriteSingleCoil(byte slaveAddress, ushort coilAddress, bool value)
        {
            var request = new WriteSingleCoilRequestResponse(slaveAddress, coilAddress, value);
            Transport.UnicastMessage<WriteSingleCoilRequestResponse>(request);
        }

        /// <summary>
        ///  异步写单个线圈。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="coilAddress">要写入起始地址</param>
        /// <param name="value">要写入的值</param>
        /// <returns>返回异步写操作的任务</returns>
        public Task WriteSingleCoilAsync(byte slaveAddress, ushort coilAddress, bool value)
        {
            var request = new WriteSingleCoilRequestResponse(slaveAddress, coilAddress, value);
            return PerformWriteRequestAsync<WriteSingleCoilRequestResponse>(request);
        }

        /// <summary>
        ///  写单个保持寄存器。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="registerAddress">要写入的起始地址</param>
        /// <param name="value">要写入的值</param>
        public void WriteSingleRegister(byte slaveAddress, ushort registerAddress, ushort value)
        {
            var request = new WriteSingleRegisterRequestResponse(slaveAddress, registerAddress, value);
            Transport.UnicastMessage<WriteSingleRegisterRequestResponse>(request);
        }

        /// <summary>
        ///  异步写入单个保持寄存器。
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="registerAddress">要写入的起始地址</param>
        /// <param name="value">要写入的值</param>
        /// <returns>返回异步写操作的任务</returns>
        public Task WriteSingleRegisterAsync(byte slaveAddress, ushort registerAddress, ushort value)
        {
            var request = new WriteSingleRegisterRequestResponse(slaveAddress, registerAddress, value);
            return PerformWriteRequestAsync<WriteSingleRegisterRequestResponse>(request);
        }

        /// <summary>
        ///  写多个保持寄存器数据（1到123，限制了数据，如要扩展需要修改）
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要写入的起始地址</param>
        /// <param name="data">要写入的值</param>
        public void WriteMultipleRegisters(byte slaveAddress, ushort startAddress, ushort[] data)
        {
            ValidateData("data", data, 123);

            var request = new WriteMultipleRegistersRequest(slaveAddress, startAddress, new RegisterCollection(data));
            Transport.UnicastMessage<WriteMultipleRegistersResponse>(request);
        }

        /// <summary>
        ///  异步写多个保持寄存器数据（1到123，限制了数据，如要扩展需要修改）
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要写入的起始地址</param>
        /// <param name="data">要写入的值</param>
        /// <returns>返回异步写操作的任务</returns>
        public Task WriteMultipleRegistersAsync(byte slaveAddress, ushort startAddress, ushort[] data)
        {
            ValidateData("data", data, 123);

            var request = new WriteMultipleRegistersRequest(slaveAddress, startAddress, new RegisterCollection(data));
            return PerformWriteRequestAsync<WriteMultipleRegistersResponse>(request);
        }

        /// <summary>
        ///  写多个线圈
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要写入的起始地址</param>
        /// <param name="data">要写入的值</param>
        public void WriteMultipleCoils(byte slaveAddress, ushort startAddress, bool[] data)
        {
            ValidateData("data", data, 1968);

            var request = new WriteMultipleCoilsRequest(slaveAddress, startAddress, new DiscreteCollection(data));
            Transport.UnicastMessage<WriteMultipleCoilsResponse>(request);
        }

        /// <summary>
        ///  异步写多个线圈
        /// </summary>
        /// <param name="slaveAddress">从设备的地址</param>
        /// <param name="startAddress">要写入的起始地址</param>
        /// <param name="data">要写入的值</param>
        /// <returns>返回异步写操作的任务</returns>
        public Task WriteMultipleCoilsAsync(byte slaveAddress, ushort startAddress, bool[] data)
        {
            ValidateData("data", data, 1968);

            var request = new WriteMultipleCoilsRequest(slaveAddress, startAddress, new DiscreteCollection(data));
            return PerformWriteRequestAsync<WriteMultipleCoilsResponse>(request);
        }

        /// <summary>
        ///   在单个Modbus事务中执行一个读操作和一个写操作的组合。写操作在读操作之前执行。
        /// </summary>
        /// <param name="slaveAddress">从设备地址</param>
        /// <param name="startReadAddress">开始读取的地址(保存寄存器从0开始寻址)</param>
        /// <param name="numberOfPointsToRead">要读取的寄存器数目</param>
        /// <param name="startWriteAddress">开始写入的地址(保存寄存器从0开始寻址)</param>
        /// <param name="writeData">需要写入的值</param>
        public ushort[] ReadWriteMultipleRegisters(byte slaveAddress, ushort startReadAddress,
            ushort numberOfPointsToRead, ushort startWriteAddress, ushort[] writeData)
        {
            ValidateNumberOfPoints("numberOfPointsToRead", numberOfPointsToRead, 125);
            ValidateData("writeData", writeData, 121);

            var request = new ReadWriteMultipleRegistersRequest(
                slaveAddress,
                startReadAddress,
                numberOfPointsToRead,
                startWriteAddress,
                new RegisterCollection(writeData));

            return PerformReadRegisters(request);
        }

        /// <summary>
        ///   在单个Modbus事务中执行一个读操作和一个写操作的组合。写操作在读操作之前执行。
        /// </summary>
        /// <param name="slaveAddress">从设备地址</param>
        /// <param name="startReadAddress">开始读取的地址(保存寄存器从0开始寻址)</param>
        /// <param name="numberOfPointsToRead">要读取的寄存器数目</param>
        /// <param name="startWriteAddress">开始写入的地址(保存寄存器从0开始寻址)</param>
        /// <param name="writeData">需要写入的值</param>
        /// <returns>返回异步操作的任务</returns>
        public Task<ushort[]> ReadWriteMultipleRegistersAsync(byte slaveAddress, ushort startReadAddress,
            ushort numberOfPointsToRead, ushort startWriteAddress, ushort[] writeData)
        {
            ValidateNumberOfPoints("numberOfPointsToRead", numberOfPointsToRead, 125);
            ValidateData("writeData", writeData, 121);

            var request = new ReadWriteMultipleRegistersRequest(
                slaveAddress,
                startReadAddress,
                numberOfPointsToRead,
                startWriteAddress,
                new RegisterCollection(writeData));

            return PerformReadRegistersAsync(request);
        }

        /// <summary>
        ///  执行自定义消息。
        /// </summary>
        /// <typeparam name="TResponse">要响应的消息类型</typeparam>
        /// <param name="request">请求消息</param>
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")]
        [SuppressMessage("Microsoft.Usage", "CA2223:MembersShouldDifferByMoreThanReturnType")]
        public TResponse ExecuteCustomMessage<TResponse>(IModbusMessage request) where TResponse : IModbusMessage, new()
        {
            return Transport.UnicastMessage<TResponse>(request);
        }

        /// <summary>
        /// 验证数据
        /// </summary>
        /// <typeparam name="T">数据类型</typeparam>
        /// <param name="argumentName">参数名称</param>
        /// <param name="data">要验证的数据</param>
        /// <param name="maxDataLength">数据最大值</param>
        private static void ValidateData<T>(string argumentName, T[] data, int maxDataLength)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            if (data.Length == 0 || data.Length > maxDataLength)
            {
                var msg = string.Format(
                    CultureInfo.InvariantCulture,
                    "The length of argument {0} must be between 1 and {1} inclusive.",
                    argumentName,
                    maxDataLength);
                throw new ArgumentException(msg);
            }
        }

        /// <summary>
        /// 验证数据数量。
        /// </summary>
        /// <param name="argumentName">参数名称</param>
        /// <param name="numberOfPoints">数据数量</param>
        /// <param name="maxNumberOfPoints">最大数据数量</param>
        private static void ValidateNumberOfPoints(string argumentName, ushort numberOfPoints, ushort maxNumberOfPoints)
        {
            if (numberOfPoints < 1 || numberOfPoints > maxNumberOfPoints)
            {
                var msg = string.Format(
                    CultureInfo.InvariantCulture,
                    "Argument {0} must be between 1 and {1} inclusive.",
                    argumentName,
                    maxNumberOfPoints);
                throw new ArgumentException(msg);
            }
        }
        /// <summary>
        /// 执行读离散量（线圈和离散输入）数据。
        /// </summary>
        /// <param name="request">读线圈、离散输入请求</param>
        /// <returns>对应数据状态</returns>
        private bool[] PerformReadDiscretes(ReadCoilsInputsRequest request)
        {
            ReadCoilsInputsResponse response = Transport.UnicastMessage<ReadCoilsInputsResponse>(request);

            return response.Data.Take(request.NumberOfPoints).ToArray();
        }

        /// <summary>
        /// 异步执行读离散量（线圈和离散输入）数据。
        /// </summary>
        /// <param name="request">读线圈、离散输入请求</param>
        /// <returns>带数据的异步操作任务</returns>
        private Task<bool[]> PerformReadDiscretesAsync(ReadCoilsInputsRequest request)
        {
            return Task.Factory.StartNew(() => PerformReadDiscretes(request));
        }

        /// <summary>
        /// 执行读保持、输入寄存器数据。
        /// </summary>
        /// <param name="request">读保持、输入寄存器请求</param>
        /// <returns>寄存器数组</returns>
        private ushort[] PerformReadRegisters(ReadHoldingInputRegistersRequest request)
        {
            ReadHoldingInputRegistersResponse response =
                Transport.UnicastMessage<ReadHoldingInputRegistersResponse>(request);

            return response.Data.Take(request.NumberOfPoints).ToArray();
        }
        /// <summary>
        /// 异步执行读保持、输入寄存器数据。
        /// </summary>
        /// <param name="request">读保持、输入寄存器请求</param>
        /// <returns>带寄存器数组的异步任务</returns>
        private Task<ushort[]> PerformReadRegistersAsync(ReadHoldingInputRegistersRequest request)
        {
            return Task.Factory.StartNew(() => PerformReadRegisters(request));
        }

        /// <summary>
        /// 执行读、写多个寄存器数据。
        /// </summary>
        /// <param name="request">读、写多个寄存器请求</param>
        /// <returns>寄存器数组</returns>
        private ushort[] PerformReadRegisters(ReadWriteMultipleRegistersRequest request)
        {
            ReadHoldingInputRegistersResponse response =
                Transport.UnicastMessage<ReadHoldingInputRegistersResponse>(request);

            return response.Data.Take(request.ReadRequest.NumberOfPoints).ToArray();
        }

        /// <summary>
        /// 执行读、写多个寄存器数据。
        /// </summary>
        /// <param name="request">读、写多个寄存器请求</param>
        /// <returns>带寄存器数组的任务</returns>
        private Task<ushort[]> PerformReadRegistersAsync(ReadWriteMultipleRegistersRequest request)
        {
            return Task.Factory.StartNew(() => PerformReadRegisters(request));
        }

        /// <summary>
        /// 异步执行自定消息类型的写入。
        /// </summary>
        /// <typeparam name="T">消息类型</typeparam>
        /// <param name="request">请求消息</param>
        /// <returns>带响应的任务</returns>
        private Task PerformWriteRequestAsync<T>(IModbusMessage request) where T : IModbusMessage, new()
        {
            return Task.Factory.StartNew(() => Transport.UnicastMessage<T>(request));
        }
    }
}
