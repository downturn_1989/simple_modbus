namespace Modbus.Device
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.IO.Ports;
    using System.Net.Sockets;
    using System.Threading.Tasks;

    using IO;

    /// <summary>
    /// Modbus IP主设备。
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1706:ShortAcronymsShouldBeUppercase", Justification = "Breaking change.")]
    public class ModbusIpMaster : ModbusMaster
    {
        private ModbusIpMaster(ModbusTransport transport)
            : base(transport)
        {
        }

        /// <summary>
        ///  Modbus IP主设备工厂方法。
        ///  TCPClient 传输。
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA1706:ShortAcronymsShouldBeUppercase", Justification = "Breaking change.")]
        public static ModbusIpMaster CreateIp(TcpClient tcpClient)
        {
            if (tcpClient == null)
                throw new ArgumentNullException("tcpClient");

            return CreateIp(new TcpClientAdapter(tcpClient));
        }

        /// <summary>
        ///  Modbus IP主设备工厂方法。
        ///  UdpClient 传输。
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA1706:ShortAcronymsShouldBeUppercase", Justification = "Breaking change.")]
        public static ModbusIpMaster CreateIp(UdpClient udpClient)
        {
            if (udpClient == null)
                throw new ArgumentNullException("udpClient");
            if (!udpClient.Client.Connected)
                throw new InvalidOperationException(Resources.UdpClientNotConnected);

            return CreateIp(new UdpClientAdapter(udpClient));
        }

        /// <summary>
        ///  Modbus IP主设备工厂方法。
        ///  SerialPort 传输。
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA1706:ShortAcronymsShouldBeUppercase", Justification = "Breaking change.")]
        public static ModbusIpMaster CreateIp(SerialPort serialPort)
        {
            if (serialPort == null)
                throw new ArgumentNullException("serialPort");

            return CreateIp(new SerialPortAdapter(serialPort));
        }

        /// <summary>
        ///  Modbus IP主设备工厂方法。
        ///  数据流对象（IStreamResource） 传输。
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA1706:ShortAcronymsShouldBeUppercase", Justification = "Breaking change.")]
        public static ModbusIpMaster CreateIp(IStreamResource streamResource)
        {
            if (streamResource == null)
                throw new ArgumentNullException("streamResource");

            return new ModbusIpMaster(new ModbusIpTransport(streamResource));
        }

        /// <summary>
        /// 读取从1到2000的线圈状态
        /// </summary>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>线圈数据</returns>
        public bool[] ReadCoils(ushort startAddress, ushort numberOfPoints)
        {
            return base.ReadCoils(Modbus.DefaultIpSlaveUnitId, startAddress, numberOfPoints);
        }

        /// <summary>
        /// 异步读取从1到2000的线圈状态   
        /// </summary>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>返回异步读操作的任务</returns>
        public Task<bool[]> ReadCoilsAsync(ushort startAddress, ushort numberOfPoints)
        {
            return base.ReadCoilsAsync(Modbus.DefaultIpSlaveUnitId, startAddress, numberOfPoints);
        }

        /// <summary>
        ///    读取1到2000离散输入的状态
        /// </summary>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>离散数据</returns>
        public bool[] ReadInputs(ushort startAddress, ushort numberOfPoints)
        {
            return base.ReadInputs(Modbus.DefaultIpSlaveUnitId, startAddress, numberOfPoints);
        }

        /// <summary>
        ///    异步读取1到2000离散输入的状态
        /// </summary>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>返回异步读操作的任务</returns>
        public Task<bool[]> ReadInputsAsync(ushort startAddress, ushort numberOfPoints)
        {
            return base.ReadInputsAsync(Modbus.DefaultIpSlaveUnitId, startAddress, numberOfPoints);
        }

        /// <summary>
        ///    读取保持寄存器数据。
        /// </summary>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>保持寄存器数据</returns>
        public ushort[] ReadHoldingRegisters(ushort startAddress, ushort numberOfPoints)
        {
            return base.ReadHoldingRegisters(Modbus.DefaultIpSlaveUnitId, startAddress, numberOfPoints);
        }

        /// <summary>
        ///    异步读取保持寄存器数据。
        /// </summary>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>返回异步读操作的任务</returns>
        public Task<ushort[]> ReadHoldingRegistersAsync(ushort startAddress, ushort numberOfPoints)
        {
            return base.ReadHoldingRegistersAsync(Modbus.DefaultIpSlaveUnitId, startAddress, numberOfPoints);
        }

        /// <summary>
        /// 读取输入寄存器数据。
        /// </summary>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>输入寄存器数据</returns>
        public ushort[] ReadInputRegisters(ushort startAddress, ushort numberOfPoints)
        {
            return base.ReadInputRegisters(Modbus.DefaultIpSlaveUnitId, startAddress, numberOfPoints);
        }

        /// <summary>
        /// 异步读取输入寄存器数据。
        /// </summary>
        /// <param name="startAddress">要读取起始地址</param>
        /// <param name="numberOfPoints">要读取的数量</param>
        /// <returns>返回异步读操作的任务</returns>
        public Task<ushort[]> ReadInputRegistersAsync(ushort startAddress, ushort numberOfPoints)
        {
            return base.ReadInputRegistersAsync(Modbus.DefaultIpSlaveUnitId, startAddress, numberOfPoints);
        }

        /// <summary>
        ///  写单个线圈。
        /// </summary>
        /// <param name="coilAddress">要写入起始地址</param>
        /// <param name="value">要写入的值</param>
        public void WriteSingleCoil(ushort coilAddress, bool value)
        {
            base.WriteSingleCoil(Modbus.DefaultIpSlaveUnitId, coilAddress, value);
        }

        /// <summary>
        ///  异步写单个线圈。
        /// </summary>
        /// <param name="coilAddress">要写入起始地址</param>
        /// <param name="value">要写入的值</param>
        /// <returns>返回异步写操作的任务</returns>
        public Task WriteSingleCoilAsync(ushort coilAddress, bool value)
        {
            return base.WriteSingleCoilAsync(Modbus.DefaultIpSlaveUnitId, coilAddress, value);
        }

        /// <summary>
        ///  写单个保持寄存器。
        /// </summary>
        /// <param name="registerAddress">要写入的起始地址</param>
        /// <param name="value">要写入的值</param>
        public void WriteSingleRegister(ushort registerAddress, ushort value)
        {
            base.WriteSingleRegister(Modbus.DefaultIpSlaveUnitId, registerAddress, value);
        }

        /// <summary>
        ///  异步写入单个保持寄存器。
        /// </summary>
        /// <param name="registerAddress">要写入的起始地址</param>
        /// <param name="value">要写入的值</param>
        /// <returns>返回异步写操作的任务</returns>
        public Task WriteSingleRegisterAsync(ushort registerAddress, ushort value)
        {
            return base.WriteSingleRegisterAsync(Modbus.DefaultIpSlaveUnitId, registerAddress, value);
        }

        /// <summary>
        ///  写多个保持寄存器数据（1到123，限制了数据，如要扩展需要修改）
        /// </summary>
        /// <param name="startAddress">要写入的起始地址</param>
        /// <param name="data">要写入的值</param>
        public void WriteMultipleRegisters(ushort startAddress, ushort[] data)
        {
            base.WriteMultipleRegisters(Modbus.DefaultIpSlaveUnitId, startAddress, data);
        }

        /// <summary>
        ///  异步写多个保持寄存器数据（1到123，限制了数据，如要扩展需要修改）
        /// </summary>
        /// <param name="startAddress">要写入的起始地址</param>
        /// <param name="data">要写入的值</param>
        /// <returns>返回异步写操作的任务</returns>
        public Task WriteMultipleRegistersAsync(ushort startAddress, ushort[] data)
        {
            return base.WriteMultipleRegistersAsync(Modbus.DefaultIpSlaveUnitId, startAddress, data);
        }
        /// <summary>
        ///  写多个线圈
        /// </summary>
        /// <param name="startAddress">要写入的起始地址</param>
        /// <param name="data">要写入的值</param>
        public void WriteMultipleCoils(ushort startAddress, bool[] data)
        {
            base.WriteMultipleCoils(Modbus.DefaultIpSlaveUnitId, startAddress, data);
        }

        /// <summary>
        ///  异步写多个线圈
        /// </summary>
        /// <param name="startAddress">要写入的起始地址</param>
        /// <param name="data">要写入的值</param>
        /// <returns>返回异步写操作的任务</returns>
        public Task WriteMultipleCoilsAsync(ushort startAddress, bool[] data)
        {
            return base.WriteMultipleCoilsAsync(Modbus.DefaultIpSlaveUnitId, startAddress, data);
        }

        /// <summary>
        ///   在单个Modbus事务中执行一个读操作和一个写操作的组合。写操作在读操作之前执行。
        /// </summary>
        /// <param name="startReadAddress">开始读取的地址(保存寄存器从0开始寻址)</param>
        /// <param name="numberOfPointsToRead">要读取的寄存器数目</param>
        /// <param name="startWriteAddress">开始写入的地址(保存寄存器从0开始寻址)</param>
        /// <param name="writeData">需要写入的值</param>
        public ushort[] ReadWriteMultipleRegisters(ushort startReadAddress, ushort numberOfPointsToRead,
            ushort startWriteAddress, ushort[] writeData)
        {
            return base.ReadWriteMultipleRegisters(Modbus.DefaultIpSlaveUnitId, startReadAddress, numberOfPointsToRead,
                startWriteAddress, writeData);
        }

        /// <summary>
        ///   在单个Modbus事务中执行一个读操作和一个写操作的组合。写操作在读操作之前执行。
        /// </summary>
        /// <param name="startReadAddress">开始读取的地址(保存寄存器从0开始寻址)</param>
        /// <param name="numberOfPointsToRead">要读取的寄存器数目</param>
        /// <param name="startWriteAddress">开始写入的地址(保存寄存器从0开始寻址)</param>
        /// <param name="writeData">需要写入的值</param>
        /// <returns>返回异步操作的任务</returns>
        public Task<ushort[]> ReadWriteMultipleRegistersAsync(ushort startReadAddress, ushort numberOfPointsToRead,
            ushort startWriteAddress, ushort[] writeData)
        {
            return base.ReadWriteMultipleRegistersAsync(Modbus.DefaultIpSlaveUnitId, startReadAddress, numberOfPointsToRead,
                startWriteAddress, writeData);
        }
    }
}
