namespace Modbus.IO
{
    using System;

    /// <summary>
    /// 数据流资源对象类。
    /// </summary>
    public interface IStreamResource : IDisposable
    {
        /// <summary>
        ///  无限延时
        /// </summary>
        int InfiniteTimeout { get; }

        /// <summary>
        ///     获取或设置读取操作未完成时，发生超时之前的毫秒数。
        /// </summary>
        int ReadTimeout { get; set; }

        /// <summary>
        ///    获取或设置在写操作未完成时，发生超时之前的毫秒数。
        /// </summary>
        int WriteTimeout { get; set; }

        /// <summary>
        ///  清除接收缓冲区。
        /// </summary>
        void DiscardInBuffer();

        /// <summary>
        ///  从输入缓冲区中读取一定数量的字节，并将这些字节写入位于指定偏移量的字节数组中。
        /// </summary>
        /// <param name="buffer">将输入写入的字节数组。</param>
        /// <param name="offset">缓冲区数组中要开始写入的偏移量。</param>
        /// <param name="count">要读取的字节数。</param>
        /// <returns>读取的字节数。</returns>
        int Read(byte[] buffer, int offset, int count);

        /// <summary>
        ///  从指定偏移量开始，将指定数量的字节从输出缓冲区写入端口
        /// </summary>
        /// <param name="buffer">包含要写入端口的数据的字节数组。</param>
        /// <param name="offset">缓冲区数组中要开始写入的偏移量。</param>
        /// <param name="count">要写入的字节数。</param>
        void Write(byte[] buffer, int offset, int count);
    }
}
