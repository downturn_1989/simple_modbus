namespace Modbus.IO
{
    using System.Diagnostics;
    using System.IO;
    using System.Text;

    using Message;
    using Utility;

    /// <summary>
    /// 串行MODBUS ASCII传输对象类，
    /// 这里在设计模式上用到了桥接模式，参考 http://en.wikipedia.org/wiki/Bridge_Pattern
    /// </summary>
    internal class ModbusAsciiTransport : ModbusSerialTransport
    {
        /// <summary>
        /// 初始化ASCII传输对象实例。
        /// </summary>
        /// <param name="streamResource">数据流资源</param>
        internal ModbusAsciiTransport(IStreamResource streamResource)
            : base(streamResource)
        {
            Debug.Assert(streamResource != null, "Argument streamResource cannot be null.");
        }

        /// <summary>
        /// 构建完整的modbus消息帧。
        /// </summary>
        /// <param name="message">modbus消息</param>
        /// <returns>数据帧数组</returns>
        internal override byte[] BuildMessageFrame(IModbusMessage message)
        {
            var msgFrame = message.MessageFrame;

            var msgFrameAscii = ModbusUtility.GetAsciiBytes(msgFrame);
            var lrcAscii = ModbusUtility.GetAsciiBytes(ModbusUtility.CalculateLrc(msgFrame));
            var nlAscii = Encoding.ASCII.GetBytes(Modbus.NewLine.ToCharArray());

            var frame = new MemoryStream(1 + msgFrameAscii.Length + lrcAscii.Length + nlAscii.Length);
            frame.WriteByte((byte)':');
            frame.Write(msgFrameAscii, 0, msgFrameAscii.Length);
            frame.Write(lrcAscii, 0, lrcAscii.Length);
            frame.Write(nlAscii, 0, nlAscii.Length);

            return frame.ToArray();
        }

        /// <summary>
        /// 校验对比
        /// </summary>
        /// <param name="message">modbus消息</param>
        /// <param name="messageFrame">需要对比的数据帧字节数组</param>
        /// <returns></returns>
        internal override bool ChecksumsMatch(IModbusMessage message, byte[] messageFrame)
        {
            return ModbusUtility.CalculateLrc(message.MessageFrame) == messageFrame[messageFrame.Length - 1];
        }

        /// <summary>
        /// 读请求
        /// </summary>
        /// <returns>消息帧字节数组</returns>
        internal override byte[] ReadRequest()
        {
            return ReadRequestResponse();
        }

        /// <summary>
        /// 读响应
        /// </summary>
        /// <typeparam name="T">modbus消息</typeparam>
        /// <returns>返回T对应的modbus消息类型的对象</returns>
        internal override IModbusMessage ReadResponse<T>()
        {
            return CreateResponse<T>(ReadRequestResponse());
        }

        /// <summary>
        /// 读请求响应数据
        /// </summary>
        /// <returns>消息帧字节数组</returns>
        internal byte[] ReadRequestResponse()
        {
            // 读取消息帧, 移除开始字符 ':'
            string frameHex = StreamResourceUtility.ReadLine(StreamResource).Substring(1);

            // 将hex字符串转byte数组
            byte[] frame = ModbusUtility.HexToBytes(frameHex);
            Debug.WriteLine("RX: {0}", string.Join(", ", frame));

            if (frame.Length < 3)
                throw new IOException("Premature end of stream, message truncated.");

            return frame;
        }
    }
}
