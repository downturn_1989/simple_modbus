namespace Modbus.IO
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;

    using Message;

    /// <summary>
    /// 串口通信协议时传输对象类。
    /// 这里在设计模式上用到了桥接模式，参考 http://en.wikipedia.org/wiki/Bridge_Pattern
    /// </summary>
    public abstract class ModbusSerialTransport : ModbusTransport
    {
        /// <summary>
        /// 检查帧，默认true。
        /// </summary>
        private bool _checkFrame = true;

        /// <summary>
        /// 初始化modbus串口传输对象实例。
        /// </summary>
        /// <param name="streamResource">数据流资源</param>
        internal ModbusSerialTransport(IStreamResource streamResource)
            : base(streamResource)
        {
            Debug.Assert(streamResource != null, "Argument streamResource cannot be null.");
        }

        /// <summary>
        ///  获取或设置一个值，该值指示是否对消息执行LRC/CRC帧检查。
        /// </summary>
        public bool CheckFrame
        {
            get { return _checkFrame; }
            set { _checkFrame = value; }
        }

        /// <summary>
        /// 释放缓冲数据。
        /// </summary>
        internal void DiscardInBuffer()
        {
            StreamResource.DiscardInBuffer();
        }

        /// <summary>
        /// 写消息数据。
        /// </summary>
        /// <param name="message">modbus消息</param>
        internal override void Write(IModbusMessage message)
        {
            DiscardInBuffer();

            byte[] frame = BuildMessageFrame(message);
            Debug.WriteLine("TX: {0}", string.Join(", ", frame));
            StreamResource.Write(frame, 0, frame.Length);
        }

        /// <summary>
        /// 创建消息应答
        /// </summary>
        /// <typeparam name="T">modbus消息类型</typeparam>
        /// <param name="frame">数据帧字节数组</param>
        /// <returns></returns>
        internal override IModbusMessage CreateResponse<T>(byte[] frame)
        {
            IModbusMessage response = base.CreateResponse<T>(frame);

            // 比较校验和
            if (CheckFrame && !ChecksumsMatch(response, frame))
            {
                string errorMessage = String.Format(CultureInfo.InvariantCulture, "Checksums failed to match {0} != {1}",
                    string.Join(", ", response.MessageFrame), string.Join(", ", frame));
                Debug.WriteLine(errorMessage);
                throw new IOException(errorMessage);
            }

            return response;
        }

        /// <summary>
        /// 校验和
        /// </summary>
        /// <param name="message"></param>
        /// <param name="messageFrame"></param>
        /// <returns></returns>
        internal abstract bool ChecksumsMatch(IModbusMessage message, byte[] messageFrame);

        /// <summary>
        /// 验证响应消息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        internal override void OnValidateResponse(IModbusMessage request, IModbusMessage response)
        {
            // no-op
        }
    }
}
