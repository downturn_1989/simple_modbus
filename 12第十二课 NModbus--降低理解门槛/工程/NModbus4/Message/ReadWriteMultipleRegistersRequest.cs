namespace Modbus.Message
{
    using System;
    using System.Globalization;
    using System.IO;

    using Data;

    /// <summary>
    /// 读写多个寄存器请求消息类。
    /// </summary>
    public class ReadWriteMultipleRegistersRequest : AbstractModbusMessage, IModbusRequest
    {
        /// <summary>
        /// 读保持寄存器、输入寄存器请求，参考<see cref="ReadHoldingInputRegistersRequest"/>
        /// </summary>
        private ReadHoldingInputRegistersRequest _readRequest;
        /// <summary>
        /// 写多个寄存器请求，参考<see cref="WriteMultipleRegistersRequest"/>
        /// </summary>
        private WriteMultipleRegistersRequest _writeRequest;

        /// <summary>
        /// 初始化读写多个寄存器请求实例。
        /// </summary>
        public ReadWriteMultipleRegistersRequest()
        {
        }

        /// <summary>
        /// 使用(从设备地址，起始地址，寄存器数量)，初始化读写多个寄存器请求实例。
        /// </summary>
        /// <param name="slaveAddress">从设备地址</param>
        /// <param name="startReadAddress">读起始地址</param>
        /// <param name="numberOfPointsToRead">要读取的寄存器数量</param>
        /// <param name="startWriteAddress">写起始地址</param>
        /// <param name="writeData">写数据集合<see cref="RegisterCollection"/></param>
        public ReadWriteMultipleRegistersRequest(byte slaveAddress, ushort startReadAddress, ushort numberOfPointsToRead,
            ushort startWriteAddress, RegisterCollection writeData)
            : base(slaveAddress, Modbus.ReadWriteMultipleRegisters)
        {
            _readRequest = new ReadHoldingInputRegistersRequest(Modbus.ReadHoldingRegisters, slaveAddress,
                startReadAddress, numberOfPointsToRead);
            _writeRequest = new WriteMultipleRegistersRequest(slaveAddress, startWriteAddress, writeData);
        }

        /// <summary>
        /// 协议单元(PDU)。
        /// </summary>
        public override byte[] ProtocolDataUnit
        {
            get
            {
                byte[] readPdu = _readRequest.ProtocolDataUnit;
                byte[] writePdu = _writeRequest.ProtocolDataUnit;
                var stream = new MemoryStream(readPdu.Length + writePdu.Length);

                stream.WriteByte(FunctionCode);
                // read and write PDUs without function codes
                stream.Write(readPdu, 1, readPdu.Length - 1);
                stream.Write(writePdu, 1, writePdu.Length - 1);

                return stream.ToArray();
            }
        }

        /// <summary>
        /// 读保持、输入寄存器请求属性。
        /// </summary>
        public ReadHoldingInputRegistersRequest ReadRequest
        {
            get { return _readRequest; }
        }

        /// <summary>
        /// 写多寄存器请求属性
        /// </summary>
        public WriteMultipleRegistersRequest WriteRequest
        {
            get { return _writeRequest; }
        }

        /// <summary>
        /// 最小帧长度
        /// </summary>
        public override int MinimumFrameSize
        {
            get { return 11; }
        }

        /// <summary>
        /// 消息转字符串。
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture,
                "Write {0} holding registers starting at address {1}, and read {2} registers starting at address {3}.",
                _writeRequest.NumberOfPoints,
                _writeRequest.StartAddress,
                _readRequest.NumberOfPoints,
                _readRequest.StartAddress);
        }

        /// <summary>
        /// 验证响应消息。
        /// </summary>
        /// <param name="response">modbus消息，参考<see cref="IModbusMessage"/></param>
        public void ValidateResponse(IModbusMessage response)
        {
            var typedResponse = (ReadHoldingInputRegistersResponse) response;

            var expectedByteCount = ReadRequest.NumberOfPoints*2;
            if (expectedByteCount != typedResponse.ByteCount)
            {
                throw new IOException(String.Format(CultureInfo.InvariantCulture,
                    "Unexpected byte count in response. Expected {0}, received {1}.",
                    expectedByteCount,
                    typedResponse.ByteCount));
            }
        }

        /// <summary>
        /// 特殊初始化。
        /// </summary>
        /// <param name="frame">帧字节数组</param>
        protected override void InitializeUnique(byte[] frame)
        {
            if (frame.Length < MinimumFrameSize + frame[10])
                throw new FormatException("Message frame does not contain enough bytes.");

            byte[] readFrame = new byte[2 + 4];
            byte[] writeFrame = new byte[frame.Length - 6 + 2];

            readFrame[0] = writeFrame[0] = SlaveAddress;
            readFrame[1] = writeFrame[1] = FunctionCode;

            Buffer.BlockCopy(frame, 2, readFrame, 2, 4);
            Buffer.BlockCopy(frame, 6, writeFrame, 2, frame.Length - 6);

            _readRequest = ModbusMessageFactory.CreateModbusMessage<ReadHoldingInputRegistersRequest>(readFrame);
            _writeRequest = ModbusMessageFactory.CreateModbusMessage<WriteMultipleRegistersRequest>(writeFrame);
        }
    }
}
