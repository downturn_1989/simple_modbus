namespace Modbus.Message
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// 从设备异常响应类。
    /// </summary>
    public class SlaveExceptionResponse : AbstractModbusMessage, IModbusMessage
    {
        /// <summary>
        /// 异常消息字典。
        /// </summary>
        private static readonly Dictionary<byte, string> _exceptionMessages = CreateExceptionMessages();

        /// <summary>
        /// 初始化从设备异常响应实例。
        /// </summary>
        public SlaveExceptionResponse()
        {
        }

        /// <summary>
        /// 使用(从设备地址，功能码，异常码)，初始化从设备异常响应实例。
        /// </summary>
        /// <param name="slaveAddress">从设备地址</param>
        /// <param name="functionCode">功能码</param>
        /// <param name="exceptionCode">异常码</param>
        public SlaveExceptionResponse(byte slaveAddress, byte functionCode, byte exceptionCode)
            : base(slaveAddress, functionCode)
        {
            SlaveExceptionCode = exceptionCode;
        }

        /// <summary>
        /// 最小帧长度。
        /// </summary>
        public override int MinimumFrameSize
        {
            get { return 3; }
        }

        /// <summary>
        /// 从设备异常码。
        /// </summary>
        public byte SlaveExceptionCode
        {
            get { return MessageImpl.ExceptionCode.Value; }
            set { MessageImpl.ExceptionCode = value; }
        }

        /// <summary>
        ///     异常消息转字符串。
        /// </summary>
        /// <returns>
        ///     返回异常消息。
        /// </returns>
        public override string ToString()
        {
            string message = _exceptionMessages.ContainsKey(SlaveExceptionCode)
                ? _exceptionMessages[SlaveExceptionCode]
                : Resources.Unknown;
            return String.Format(CultureInfo.InvariantCulture, Resources.SlaveExceptionResponseFormat,
                Environment.NewLine, FunctionCode, SlaveExceptionCode, message);
        }

        /// <summary>
        /// 创建异常消息字典。
        /// </summary>
        /// <returns>返回消息字典。</returns>
        internal static Dictionary<byte, string> CreateExceptionMessages()
        {
            Dictionary<byte, string> messages = new Dictionary<byte, string>(9);

            messages.Add(1, Resources.IllegalFunction);
            messages.Add(2, Resources.IllegalDataAddress);
            messages.Add(3, Resources.IllegalDataValue);
            messages.Add(4, Resources.SlaveDeviceFailure);
            messages.Add(5, Resources.Acknowlege);
            messages.Add(6, Resources.SlaveDeviceBusy);
            messages.Add(8, Resources.MemoryParityError);
            messages.Add(10, Resources.GatewayPathUnavailable);
            messages.Add(11, Resources.GatewayTargetDeviceFailedToRespond);

            return messages;
        }

        /// <summary>
        /// 特殊初始化。
        /// </summary>
        /// <param name="frame">帧字节数组</param>
        protected override void InitializeUnique(byte[] frame)
        {
            if (FunctionCode <= Modbus.ExceptionOffset)
                throw new FormatException(Resources.SlaveExceptionResponseInvalidFunctionCode);

            SlaveExceptionCode = frame[2];
        }
    }
}
