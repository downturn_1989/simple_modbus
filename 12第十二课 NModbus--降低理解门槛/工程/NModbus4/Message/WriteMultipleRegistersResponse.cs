namespace Modbus.Message
{
    using System;
    using System.Globalization;
    using System.Net;

    /// <summary>
    /// 写多个寄存器响应类。
    /// </summary>
    public class WriteMultipleRegistersResponse : AbstractModbusMessage, IModbusMessage
    {
        /// <summary>
        /// 初始化写多个寄存器响应实例。
        /// </summary>
        public WriteMultipleRegistersResponse()
        {
        }

        /// <summary>
        /// 使用(从设备地址，起始地址，寄存器数量)，初始化写多个寄存器响应实例。
        /// </summary>
        /// <param name="slaveAddress">从设备地址</param>
        /// <param name="startAddress">起始地址</param>
        /// <param name="numberOfPoints">寄存器的数量</param>
        public WriteMultipleRegistersResponse(byte slaveAddress, ushort startAddress, ushort numberOfPoints)
            : base(slaveAddress, Modbus.WriteMultipleRegisters)
        {
            StartAddress = startAddress;
            NumberOfPoints = numberOfPoints;
        }

        /// <summary>
        /// 寄存器的数量
        /// </summary>
        public ushort NumberOfPoints
        {
            get { return MessageImpl.NumberOfPoints.Value; }
            set
            {
                if (value > Modbus.MaximumRegisterRequestResponseSize)
                {
                    throw new ArgumentOutOfRangeException("NumberOfPoints",
                        String.Format(CultureInfo.InvariantCulture, "Maximum amount of data {0} registers.",
                            Modbus.MaximumRegisterRequestResponseSize));
                }

                MessageImpl.NumberOfPoints = value;
            }
        }

        /// <summary>
        /// 起始地址
        /// </summary>
        public ushort StartAddress
        {
            get { return MessageImpl.StartAddress.Value; }
            set { MessageImpl.StartAddress = value; }
        }

        /// <summary>
        /// 最小帧长度
        /// </summary>
        public override int MinimumFrameSize
        {
            get { return 6; }
        }

        /// <summary>
        /// 消息转字符串
        /// </summary>
        /// <returns>返回消息字符串</returns>
        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture, "Wrote {0} holding registers starting at address {1}.",
                NumberOfPoints, StartAddress);
        }

        /// <summary>
        /// 特殊初始化
        /// </summary>
        /// <param name="frame">帧字节数组</param>
        protected override void InitializeUnique(byte[] frame)
        {
            StartAddress = (ushort) IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 2));
            NumberOfPoints = (ushort) IPAddress.NetworkToHostOrder(BitConverter.ToInt16(frame, 4));
        }
    }
}
