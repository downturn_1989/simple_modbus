namespace Modbus.Data
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    /// <summary>
    ///  Modbus数据模型的原始集合。
    /// </summary>
    public class ModbusDataCollection<TData> : Collection<TData>
    {
        /// <summary>
        /// 是否允许元素是空，默认true
        /// </summary>
        private bool _allowZeroElement = true;

        /// <summary>
        ///  初始化数据集合实例。
        /// </summary>
        public ModbusDataCollection()
        {
            AddDefault(this);
            _allowZeroElement = false;
        }

        /// <summary>
        ///  使用相关数据数组初始化数据集合实例。
        /// </summary>
        /// <param name="data">相关数据数组</param>
        public ModbusDataCollection(params TData[] data)
            : this((IList<TData>) data)
        {
        }

        /// <summary>
        ///  使用相关数据列表初始化数据集合实例。
        /// </summary>
        /// <param name="data">数据列表</param>
        public ModbusDataCollection(IList<TData> data)
            : base(AddDefault(data.IsReadOnly ? new List<TData>(data) : data))
        {
            _allowZeroElement = false;
        }

        /// <summary>
        /// 获取、设置modbus 数据类型
        /// </summary>
        internal ModbusDataType ModbusDataType { get; set; }

        /// <summary>
        /// 将默认元素添加到集合中。
        /// </summary>
        /// <param name="data">对应数据列表</param>
        /// <returns>数据列表</returns>
        private static IList<TData> AddDefault(IList<TData> data)
        {
            data.Insert(0, default(TData));
            return data;
        }

        /// <summary>
        ///    对象中特殊位置插入一个元素 <see cref="T:System.Collections.ObjectModel.Collection`1"></see> 
        /// </summary>
        /// <param name="index">应该插入项的从零开始的索引。</param>
        /// <param name="item">要插入的对象。对于引用类型，该值可以为空。</param>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        ///    Index小于零。-or-index大于
        ///     <see cref="P:System.Collections.ObjectModel.Collection`1.Count"></see>.
        /// </exception>
        protected override void InsertItem(int index, TData item)
        {
            if (!_allowZeroElement && index == 0)
                throw new ArgumentOutOfRangeException("index", "0 is not a valid address for a Modbus data collection.");

            base.InsertItem(index, item);
        }

        /// <summary>
        ///  替换指定索引处的元素。
        /// </summary>
        /// <param name="index">要替换的元素的从零开始的索引。</param>
        /// <param name="item">元素在指定索引处的新值。对于引用类型，该值可以为空。</param>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        ///    Index小于零。-or-index大于 <see cref="P:System.Collections.ObjectModel.Collection ' 1.Count"/>
        /// </exception>
        protected override void SetItem(int index, TData item)
        {
            if (index == 0)
                throw new ArgumentOutOfRangeException("index", "0 is not a valid address for a Modbus data collection.");

            base.SetItem(index, item);
        }

        /// <summary>
        ///     元素的指定索引处的元素<see cref="T:System.Collections.ObjectModel.Collection`1"></see>.
        /// </summary>
        /// <param name="index">要删除的元素的从零开始的索引。</param>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        ///    Index小于零。-or-index等于或大于
        ///     <see cref="P:System.Collections.ObjectModel.Collection`1.Count"></see>.
        /// </exception>
        protected override void RemoveItem(int index)
        {
            if (index == 0)
                throw new ArgumentOutOfRangeException("index", "0 is not a valid address for a Modbus data collection.");

            base.RemoveItem(index);
        }

        /// <summary>
        /// 对象中删除所有元素 <see cref="T:System.Collections.ObjectModel.Collection`1"></see>.
        /// </summary>
        protected override void ClearItems()
        {
            _allowZeroElement = true;
            base.ClearItems();
            AddDefault(this);
            _allowZeroElement = false;
        }
    }
}
