namespace Modbus.Data
{
    /// <summary>
    /// 数据存储工厂
    /// </summary>
    public static class DataStoreFactory
    {
        /// <summary>
        ///  默认数据存储寄存器值设置为0，离散值设置为false的工厂方法。
        /// </summary>
        public static DataStore CreateDefaultDataStore()
        {
            return CreateDefaultDataStore(ushort.MaxValue, ushort.MaxValue, ushort.MaxValue, ushort.MaxValue);
        }

        /// <summary>
        ///  默认数据存储寄存器值设置为0，离散值设置为false的工厂方法。
        /// </summary>
        /// <param name="coilsCount">线圈数量</param>
        /// <param name="inputsCount">离散输入数量</param>
        /// <param name="holdingRegistersCount">保持寄存器数量</param>
        /// <param name="inputRegistersCount">输入寄存器数量</param>
        public static DataStore CreateDefaultDataStore(ushort coilsCount, ushort inputsCount, ushort holdingRegistersCount, ushort inputRegistersCount)
        {
            var coils = new bool[coilsCount];
            var inputs = new bool[inputsCount];
            var holdingRegs = new ushort[holdingRegistersCount];
            var inputRegs = new ushort[inputRegistersCount];

            return new DataStore(coils, inputs, holdingRegs, inputRegs);
        }

        /// <summary>
        /// 测试数据存储的工厂方法。
        /// </summary>
        internal static DataStore CreateTestDataStore()
        {
            DataStore dataStore = new DataStore();

            for (int i = 1; i < 3000; i++)
            {
                bool value = i%2 > 0;
                dataStore.CoilDiscretes.Add(value);
                dataStore.InputDiscretes.Add(!value);
                dataStore.HoldingRegisters.Add((ushort) i);
                dataStore.InputRegisters.Add((ushort) (i*10));
            }

            return dataStore;
        }
    }
}
