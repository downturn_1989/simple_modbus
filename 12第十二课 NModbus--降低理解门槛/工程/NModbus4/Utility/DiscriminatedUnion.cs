﻿namespace Modbus.Utility
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;

    /// <summary>
    ///  可区分联合的选项枚举结构
    /// </summary>
    public enum DiscriminatedUnionOption
    {
        /// <summary>
        ///     Option A
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "A")] A,

        /// <summary>
        ///     Option B
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "B")] B
    }

    /// <summary>
    ///  可区分联合类，用于解决离散量Bool型数据和Ushort数据选择问题
    /// </summary>
    /// <typeparam name="TA">TA的数据类型。</typeparam>
    /// <typeparam name="TB">TB的数据类型。</typeparam>
    /// '
    public class DiscriminatedUnion<TA, TB>
    {
        /// <summary>
        /// TA类型数据
        /// </summary>
        private TA optionA;
        /// <summary>
        /// TB类型数据
        /// </summary>
        private TB optionB;
        /// <summary>
        /// 可区分联合的选项
        /// </summary>
        private DiscriminatedUnionOption option;

        /// <summary>
        /// 获取A类型的数据。
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "A")]
        public TA A
        {
            get
            {
                if (this.Option != DiscriminatedUnionOption.A)
                    throw new InvalidOperationException(String.Format(CultureInfo.InvariantCulture,
                        "{0} is not a valid option for this discriminated union instance.", DiscriminatedUnionOption.A));

                return this.optionA;
            }
        }

        /// <summary>
        /// 获取B类型的数据。
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "B")]
        public TB B
        {
            get
            {
                if (this.Option != DiscriminatedUnionOption.B)
                    throw new InvalidOperationException(String.Format(CultureInfo.InvariantCulture,
                        "{0} is not a valid option for this discriminated union instance.", DiscriminatedUnionOption.B));

                return this.optionB;
            }
        }

        /// <summary>
        /// 可区分联合的选项属性
        /// </summary>
        public DiscriminatedUnionOption Option
        {
            get { return this.option; }
        }

        /// <summary>
        /// 创建带有TA数据类型的工厂方法。
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes",
            Justification = "Factory method.")]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "0#a")]
        public static DiscriminatedUnion<TA, TB> CreateA(TA a)
        {
            return new DiscriminatedUnion<TA, TB>() {option = DiscriminatedUnionOption.A, optionA = a};
        }

        /// <summary>
        /// 创建带有TA数据类型的工厂方法。
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes",
            Justification = "Factory method.")]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "0#b")]
        public static DiscriminatedUnion<TA, TB> CreateB(TB b)
        {
            return new DiscriminatedUnion<TA, TB>() {option = DiscriminatedUnionOption.B, optionB = b};
        }

        /// <summary>
        ///  数据类型字符串
        /// </summary>
        /// <returns>
        /// 返回当前使用类型。
        /// </returns>
        public override string ToString()
        {
            string value = null;
            switch (Option)
            {
                case DiscriminatedUnionOption.A:
                    value = A.ToString();
                    break;
                case DiscriminatedUnionOption.B:
                    value = B.ToString();
                    break;
            }
            return value;
        }
    }
}
